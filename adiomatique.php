<?php
/**
 * Plugin Name: Adiomatique
 * Description: Einfache Terminverwaltung.
 * Version: 17
 * Author: Vitali Homenko
 * Author URI: mailto:vitali.homenko@gmail.com
 * License: GPL-3.0
 *
 *
 * Adiomatique – simple event management for WordPress
 * Copyright (C) 2017 Vitali Homenko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace adi;

defined( 'ABSPATH' ) or die( '' );

define( 'NEWS_CAT_ID', get_option( 'default_category' ) );
define( 'START_PAGE_ID', get_option( 'page_on_front' ) );

const TZ = 'Europe/Berlin';
date_default_timezone_set( TZ );

require_once( 'EventManager.php' );
require_once( 'tweaks.php' );

if( is_admin() ) {
	require_once( 'admin_page.php' );
	require_once( 'admin_post.php' );
} else {
	require_once( 'show.php' );
}
